FROM python:3.6

WORKDIR /app/
COPY requirements.txt /app/
RUN pip3 install -r requirements.txt

#CMD ["python manage.py runserver 0:8090"]
