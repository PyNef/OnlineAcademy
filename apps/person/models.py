# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User

from OnlineAcademy.choices import STATUS_CHOICES, PROFILE_CHOICES, GENDER_CHOICES



class Person(models.Model):
    '''
    All users in general are related to a person for information general.
    '''
    dni = models.CharField(u'D.N.I.', max_length=8, unique=True, null= True, blank= True)
    name = models.CharField(u'Nombres', max_length=150)
    last_name = models.CharField(u'Apellido paterno', max_length=100)
    mothers_last_name = models.CharField(u'Apellido materno', max_length=100)
    email = models.EmailField(u'Correo Electronico', max_length=254, null= True, blank= True)
    number_phone = models.CharField(u'Número telefónico', max_length=50, null= True, blank= True)
    gender = models.CharField(u'Genero', max_length=10, choices=GENDER_CHOICES, default='F')
    status_id = models.CharField(u'Estado', max_length=10, choices=STATUS_CHOICES, default='A', db_index=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null= True, blank= True)
    creation_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null=True, blank=True)
    update_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null= True, blank= True)

    def __unicode__(self):
        return u'%s %s, %s' % (
                                self.last_name.capitalize(),
                                self.mothers_last_name.capitalize(),
                                self.name.capitalize()
                                )

    class Meta:
        ordering = ['last_name','mothers_last_name','name']
        verbose_name = u'Person'
        verbose_name_plural = u'Persons'



class Profile(models.Model):
    '''
    The people who will use the system will have at least one profile affiliated.
    '''
    user = models.OneToOneField(User, on_delete=models.PROTECT, null=True, blank=True)
    person = models.OneToOneField('Person', on_delete=models.PROTECT, null=True, blank=True)
    type_profile = models.CharField(max_length=7, choices=PROFILE_CHOICES, default='NORMAL')
    status_id = models.CharField(u'Estado', max_length=10, choices=STATUS_CHOICES, default='A', db_index=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null= True, blank= True)
    creation_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null=True, blank=True)
    update_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null= True, blank= True)

    def __unicode__(self):
        return u'Perfil de %s' % (self.user.username)


    class Meta:
        ordering = ['person']
        verbose_name = u'Profile'
        verbose_name_plural = u'Profiles'
