from django.contrib import admin

from apps.person.models import Person
from apps.person.models import Profile

admin.site.register(Person)
admin.site.register(Profile)
