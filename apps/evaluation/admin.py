from django.contrib import admin

from apps.evaluation.models import Question
from apps.evaluation.models import Alternative
from apps.evaluation.models import Eta
from apps.evaluation.models import EtaCategory
from apps.evaluation.models import EtaQuestion

admin.site.register(Question)
admin.site.register(Alternative)
admin.site.register(Eta)
admin.site.register(EtaCategory)
admin.site.register(EtaQuestion)
