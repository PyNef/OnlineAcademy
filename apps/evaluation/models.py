from django.db import models
from django.contrib.auth.models import User

from apps.institution.models import Area, Course, University
from OnlineAcademy.choices import STATUS_CHOICES
from OnlineAcademy.choices import LEVELS_CHOICES
from OnlineAcademy.choices import TYPE_QUESTION_CHOICES

from ckeditor.fields import RichTextField

class Question(models.Model):
    '''
    Is one bank of questions for the evaluations.
    '''
    description = RichTextField(u'Descripción')
    answer = models.CharField(u'Respuesta', max_length=100)
    type_question = models.CharField(u'Tipo', max_length=10, choices=TYPE_QUESTION_CHOICES, default='ETA')
    level = models.CharField(u'Nivel', max_length=10, choices=LEVELS_CHOICES, default='C')
    university = models.ForeignKey(University, null=True, blank=True, on_delete=models.PROTECT)
    area = models.ForeignKey(Area, null=True, blank=True, on_delete=models.PROTECT)
    course = models.ForeignKey(Course, null=True, blank=True, on_delete=models.PROTECT)
    status_id = models.CharField(u'Estado', max_length=10, choices=STATUS_CHOICES, default='A', db_index=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null= True, blank= True)
    creation_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null=True, blank=True)
    update_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null= True, blank= True)

    def __unicode__(self):
        return u'%s -> %s' % (self.description, self.answer)

    def get_data_json(self):
        the_alternatives = Alternative.objects.filter(question_id=self.id)
        alternatives = [alternative.get_data_json() for alternative in the_alternatives]
        data = {
            'answer': self.answer,
            'description': self.description,
            'tipo_question': self.type_question,
            'university': self.university.name,
            'course': self.course.name,
            'level': self.level,
            'area': self.area.name,
            'alternatives': alternatives,
        }
        return data


    class Meta:
        ordering = ['pk']
        verbose_name = u'Question'
        verbose_name_plural = u'Questions'


class Alternative(models.Model):
    '''
    Is one bank of alternatives for the questions.
    '''
    question = models.ForeignKey(Question, on_delete=models.PROTECT, null=True, blank=True)
    description = models.CharField(u'Descripción', max_length=150)
    status_id = models.CharField(u'Estado', max_length=10, choices=STATUS_CHOICES, default='A', db_index=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null= True, blank= True)
    creation_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null=True, blank=True)
    update_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null= True, blank= True)

    def __unicode__(self):
        return u'%s' % (self.description)

    def get_data_json(self):
        data = {
            'description': self.description,
        }
        return data

    class Meta:
        ordering = ['pk']
        verbose_name = u'Alternative'
        verbose_name_plural = u'Alternatives'


class Eta(models.Model):
    '''
    ETA -> Examen tipo Admisión
    '''
    name = models.CharField(u'Descripción', max_length=150)
    resolution_time = models.IntegerField()
    description = models.CharField(u'Descripción', max_length=150)
    open_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    close_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    status_id = models.CharField(u'Estado', max_length=10, choices=STATUS_CHOICES, default='A', db_index=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null= True, blank= True)
    creation_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null=True, blank=True)
    update_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null= True, blank= True)
    # categories: Array<EtaCategory>

    def __unicode__(self):
        return u'%s' % (self.name)

    def get_data_json(self):
        the_categories = EtaCategory.objects.filter(eta_id=self.id)
        categories = [categorie.get_data_json() for categorie in the_categories]
        data = {
            'name': self.name,
            'resolution_time': self.resolution_time,
            'description': self.description,
            # 'open_at': self.open_at,
            # 'close_at': self.close_at,
            'categorie': categories,
        }
        return data

    class Meta:
        ordering = ['pk']
        verbose_name = u'ETA'
        verbose_name_plural = u'ETAs'


class EtaCategory(models.Model):
    '''
    Category in eta
    '''
    eta = models.ForeignKey(Eta, related_name='categories', on_delete=models.PROTECT, null=True, blank=True)
    name = models.CharField(u'Nombre', max_length=150)
    description = models.CharField(u'Descripción', max_length=150)
    status_id = models.CharField(u'Estado', max_length=10, choices=STATUS_CHOICES, default='A', db_index=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null= True, blank= True)
    creation_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null=True, blank=True)
    update_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null= True, blank= True)

    def __unicode__(self):
        return u'%s' % (self.name)

    def get_data_json(self):
        the_questions = EtaQuestion.objects.filter(category_id=self.id)
        questions = [question.get_data_json() for question in the_questions]
        data = {
            'name': self.name,
            'description': self.description,
            'questions': questions
        }
        return data

    class Meta:
        ordering = ['pk']
        verbose_name = u'Category'
        verbose_name_plural = u'Categories'


class EtaQuestion(models.Model):
    '''
    Binding Question to eta
    '''
    category = models.ForeignKey(EtaCategory, on_delete=models.PROTECT, null=True, blank=True)
    question = models.ForeignKey(Question, on_delete=models.PROTECT, null=True, blank=True)
    status_id = models.CharField(u'Estado', max_length=10, choices=STATUS_CHOICES, default='A', db_index=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null= True, blank= True)
    creation_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null=True, blank=True)
    update_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null= True, blank= True)

    def __unicode__(self):
        return u'%s' % ('q1')

    def get_data_json(self):
        data = {
            'question': self.question.get_data_json(),
        }
        return data

    class Meta:
        ordering = ['pk']
        verbose_name = u'etaQuestion'
        verbose_name_plural = u'etaQuestions'
