from django.db import models
from django.contrib.auth.models import User

from OnlineAcademy.choices import STATUS_CHOICES



class University(models.Model):
    '''
    All the universitys that use.
    '''
    name = models.CharField(u'Nombre', max_length=100)
    description = models.TextField(u'Descripción')
    status_id = models.CharField(u'Estado', max_length=10, choices=STATUS_CHOICES, default='A', db_index=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null= True, blank= True)
    creation_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null=True, blank=True)
    update_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null= True, blank= True)

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        ordering = ['name']
        verbose_name = u'University'
        verbose_name_plural = u'Universitys'


class Area(models.Model):
    '''
    All the areas that use.
    '''
    name = models.CharField(u'Nombre', max_length=100)
    description = models.TextField(u'Descripción')
    status_id = models.CharField(u'Estado', max_length=10, choices=STATUS_CHOICES, default='A', db_index=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null= True, blank= True)
    creation_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null=True, blank=True)
    update_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null= True, blank= True)

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        ordering = ['name']
        verbose_name = u'Area'
        verbose_name_plural = u'Areas'


class Course(models.Model):
    '''
    All the courses that use.
    '''
    name = models.CharField(u'Nombre', max_length=100)
    description = models.TextField(u'Descripción')
    status_id = models.CharField(u'Estado', max_length=10, choices=STATUS_CHOICES, default='A', db_index=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null= True, blank= True)
    creation_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null=True, blank=True)
    update_user = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT, null= True, blank= True)

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        ordering = ['name']
        verbose_name = u'Course'
        verbose_name_plural = u'Courses'
