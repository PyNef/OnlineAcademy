from django.contrib import admin

from apps.institution.models import University
from apps.institution.models import Area
from apps.institution.models import Course

admin.site.register(University)
admin.site.register(Area)
admin.site.register(Course)
