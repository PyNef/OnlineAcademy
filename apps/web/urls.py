from django.urls import path

from . import views
from apps.web.views import HomeView, RegisterView

urlpatterns = [
    path('', HomeView.as_view(), name='web_home'),
    path('accounts/register', RegisterView.as_view(), name='web_register'),
]
