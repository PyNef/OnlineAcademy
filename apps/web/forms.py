# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from apps.person.models import *

class UserForm(UserCreationForm):
    username = forms.EmailField(required= True)
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['password1'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['password2'].widget.attrs = {
            'class': 'form-control',
        }
    class Meta:
        model = User
        fields = ('username', 'password1', 'password2', )
        widgets = {
            'email': forms.EmailInput(),
        }
