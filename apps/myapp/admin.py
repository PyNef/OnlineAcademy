from django.contrib import admin

from apps.myapp.models import *

admin.site.register(ServiceType)
admin.site.register(Level)
admin.site.register(University)
admin.site.register(Especialty)
admin.site.register(Program)
admin.site.register(SubjectMatter)
admin.site.register(Course)
admin.site.register(Theme)
admin.site.register(Topical)
admin.site.register(Video)
admin.site.register(archivo)
admin.site.register(EvaluationTopical)
admin.site.register(Foro)
