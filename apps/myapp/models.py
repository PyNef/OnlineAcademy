from django.db import models
from apps.evaluation.models import *

class ServiceType(models.Model):
    service_type = models.CharField(u'Tipo de Servicio', max_length=8, unique=True, null=True, blank=True)
    # university = models.ForeignKey(University, on_delete=models.PROTECT)

class Level(models.Model):
    '''
    Es el grado de nivel del curso: basico, intermedio, avanzado y experto
    '''
    level = models.CharField(u'Nivel', max_length=8, unique=True, null=True, blank=True)

class University(models.Model):
    university = models.CharField(u'Universidad', max_length=8, unique=True, null=True, blank=True)

class Especialty(models.Model):
    especialty = models.CharField(u'Facultad', max_length=8, unique=True, null=True, blank=True)

class Program(models.Model):
    name = models.CharField(u'Nombre', max_length=8, unique=True, null=True, blank=True)
    service_type = models.ForeignKey(ServiceType, on_delete=models.PROTECT, null=True, blank=True)
    especialty = models.ForeignKey(Especialty, on_delete=models.PROTECT, null=True, blank=True)
    university = models.ForeignKey(University, on_delete=models.PROTECT, null=True, blank=True)

class SubjectMatter(models.Model):
    subject_matter = models.CharField(u'Materia', max_length=150, unique=True, null=True, blank=True)
    program = models.ForeignKey(Program, on_delete=models.PROTECT, null=True, blank=True)

class Course(models.Model):
    course = models.CharField(u'Curso', max_length=150, null=True, blank=True)
    level = models.ForeignKey(Level, on_delete=models.PROTECT, null=True, blank=True)
    subject_matter = models.ForeignKey(SubjectMatter, on_delete=models.PROTECT, null=True, blank=True)

class Theme(models.Model):
    theme = models.CharField(u'Tema', max_length=150, null=True, blank=True)
    course = models.ForeignKey(Course, on_delete=models.PROTECT, null=True, blank=True)

class Topical(models.Model):
    topical = models.CharField(u'Topico', max_length=150, null=True, blank=True)
    theme = models.ForeignKey(Theme, on_delete=models.PROTECT, null=True, blank=True)

class Video(models.Model):
    video = models.CharField(u'Video', max_length=250, null=True, blank=True)
    topical = models.ForeignKey(Topical, on_delete=models.PROTECT, null=True, blank=True)

class archivo(models.Model):
    archivo = models.CharField(u'PDF', max_length=250, null=True, blank=True)
    topical = models.ForeignKey(Topical, on_delete=models.PROTECT, null=True, blank=True)

class EvaluationTopical(models.Model):
    evaluation_topical = models.CharField(u'Evaluacion', max_length=250, null=True, blank=True)
    topical = models.ForeignKey(Topical, on_delete=models.PROTECT, null=True, blank=True)

class Foro(models.Model):
    foro = models.CharField(u'Foro', max_length=250, null=True, blank=True)
    topical = models.ForeignKey(Topical, on_delete=models.PROTECT, null=True, blank=True)
