# Generated by Django 2.0 on 2018-02-24 21:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('service_type', models.CharField(blank=True, max_length=8, null=True, unique=True, verbose_name='Tipo de Servicio')),
            ],
        ),
        migrations.RemoveField(
            model_name='especialty',
            name='nombre',
        ),
        migrations.RemoveField(
            model_name='program',
            name='type',
        ),
        migrations.RemoveField(
            model_name='university',
            name='nombre',
        ),
        migrations.AddField(
            model_name='especialty',
            name='especialty',
            field=models.CharField(blank=True, max_length=8, null=True, unique=True, verbose_name='Facultad'),
        ),
        migrations.AddField(
            model_name='university',
            name='university',
            field=models.CharField(blank=True, max_length=8, null=True, unique=True, verbose_name='Universidad'),
        ),
        migrations.AlterField(
            model_name='program',
            name='especialty',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='myapp.Especialty'),
        ),
        migrations.AlterField(
            model_name='program',
            name='university',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='myapp.University'),
        ),
        migrations.DeleteModel(
            name='ProgramType',
        ),
        migrations.AddField(
            model_name='program',
            name='is_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='myapp.ServiceType'),
        ),
    ]
